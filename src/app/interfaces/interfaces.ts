export interface Info {
  correo?: string;
  celular?: any;
  pin?: number;
  fecha?: string;
  radicado?: string;
  tipo?: string;
}

export interface Conversacion {
  comentario?: string;
  fecha?: string;
  operador?: string;
  doc?: boolean;
}

export interface RSInfo {
  info?: Info;
  flag?: boolean;
  conversacion?: Conversacion[];
}
