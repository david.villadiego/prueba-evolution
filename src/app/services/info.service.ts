import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RSInfo} from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  constructor(private http: HttpClient) {
  }

  serviceInfoPqr() {
    return this.http.get<RSInfo>('assets/data/info.json');
  }
}
