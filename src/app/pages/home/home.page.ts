import {Component, OnInit} from '@angular/core';
import {InfoService} from '../../services/info.service';
import {RSInfo} from '../../interfaces/interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.css']
})
export class HomeComponent implements OnInit {
  infoPqr: RSInfo;
  sendInfo: RSInfo;
  workFlow: RSInfo;

  constructor(private infoServices: InfoService) {
  }

  ngOnInit(): void {
    this.getInfoServices();
  }

  getInfoServices(): void {
    this.infoServices.serviceInfoPqr().subscribe((res: RSInfo) => {
      this.infoPqr = res;
    }, error => {
      console.log(error);
    });
  }

}
