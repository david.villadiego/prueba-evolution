import {Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef, Renderer2} from '@angular/core';
import {RSInfo} from '../../interfaces/interfaces';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  @Input() infoPqrArray: RSInfo[];
  @Output() dataPqr = new EventEmitter<RSInfo>();
  item: any;

  constructor() {
  }

  ngOnInit(): void {

  }

  sendInfo(data: RSInfo, myLink): void {
    const elemento = document.getElementsByClassName('list-group-item');
    for (let i = 0; i < elemento.length; i++) {
      elemento[i].classList.remove('active');
    }
    myLink.classList.add('active');
    this.dataPqr.emit(data);
  }
}
