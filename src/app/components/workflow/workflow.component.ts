import {Component, Input, OnInit} from '@angular/core';
import {RSInfo} from '../../interfaces/interfaces';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.css']
})
export class WorkflowComponent implements OnInit {
  @Input() workFlow: RSInfo;

  constructor() {
  }

  ngOnInit(): void {
  }

  parImpar(numero): boolean {
    if (numero % 2 === 0) {
      return true;
    } else {
      return false;
    }
  }

  clearInfo(): void {
    this.workFlow.flag = false;
    console.log(this.workFlow);
  }
}
