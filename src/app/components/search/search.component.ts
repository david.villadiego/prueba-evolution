import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RSInfo} from '../../interfaces/interfaces';
import {$e} from 'codelyzer/angular/styles/chars';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Input() infoPqr: RSInfo[];
  @Output() sendDataPqr = new EventEmitter<RSInfo>();
  infoPqrArray: RSInfo[] = [];
  dataPqr: RSInfo;

  constructor() {
  }

  ngOnInit(): void {
  }


  searchInfoPqr(value: string): void {
    this.infoPqrArray = [];
    const term = value.toLowerCase();
    for (const info of this.infoPqr) {
      const radicado = info.info.radicado.toLowerCase();
      const tipo = info.info.tipo.toLowerCase();
      if (radicado.indexOf(term) >= 0) {
        this.infoPqrArray.push(info);
      } else if (tipo.indexOf(term) >= 0) {
        this.infoPqrArray.push(info);
      }
    }
  }

  sendInfoPqr($event: RSInfo): void {
    this.sendDataPqr.emit($event);
  }
}
