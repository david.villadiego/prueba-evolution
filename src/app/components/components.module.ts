import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchComponent} from './search/search.component';
import { FormInfoComponent } from './form-info/form-info.component';
import { SearchResultComponent } from './search-result/search-result.component';
import {FormsModule} from '@angular/forms';
import { WorkflowComponent } from './workflow/workflow.component';
import { AlertComponent } from './alert/alert.component';


@NgModule({
  entryComponents: [SearchComponent],
  declarations: [SearchComponent, FormInfoComponent, SearchResultComponent, WorkflowComponent, AlertComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [SearchComponent, FormInfoComponent, WorkflowComponent]
})
export class ComponentsModule {
}
