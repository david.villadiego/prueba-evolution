import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input() info: boolean;
  @Input() danger: boolean;
  @Input() textInfo: string;
  @Input() textDanger: string;


  constructor() {
console.log(this.info)
  }

  ngOnInit(): void {
  }

}
