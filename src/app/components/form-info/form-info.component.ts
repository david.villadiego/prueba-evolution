import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Conversacion, RSInfo} from '../../interfaces/interfaces';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-form-info',
  templateUrl: './form-info.component.html',
  styleUrls: ['./form-info.component.css']
})
export class FormInfoComponent implements OnInit {
  @Input() dataForm: RSInfo;
  @Output() sendWorkFlow = new EventEmitter<RSInfo>();
  flagData: boolean;

  constructor() {
    this.dataForm = {};
    this.dataForm.info = {};
    this.flagData = true;

  }

  ngOnInit(): void {

  }

  onSubmit(formulario: NgForm): void {
    console.log(formulario);
    if (formulario.valid) {
      if (formulario.form.value.celular || formulario.form.value.correo) {
        this.flagData = true;
        this.dataForm.flag = true;
        this.sendWorkFlow.emit(this.dataForm);
      } else {
        this.flagData = false;
        return;
      }
    } else {
      this.flagData = false;
    }
  }
}
